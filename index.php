<html>
  <head>
    <title>Paulo Silva</title>
    <meta property="og:description" content="Paulo Silva website." />
    <meta property="og:site_name" content="Paulo Silva" />
    <meta name="theme-color" content="#f39c12" />

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Dosis:800|Leckerli+One|Cabin+Sketch:700" />
    <link rel="stylesheet" href="data/css/content.css" />
    <link rel="stylesheet" href="data/css/page.css" />
    <link rel="stylesheet" href="data/css/menu.css" />
    <link rel="stylesheet" href="data/css/animation.css" />
    <link rel="stylesheet" href="data/css/phone.css" />
    <link rel="stylesheet" href="data/css/account.css" />
    <link rel="icon" href="data/media/favicon.png" />

    <script src="data/js/jquery.js"></script>
    <script src="data/js/loading.js"></script>
    <script src="data/js/menu.js"></script>
    <script src="data/js/page.js"></script>
    <script src="data/js/animation.js"></script>
    <script src="data/js/account.js"></script>
    <script>
      $.get("api/posts/get", {a: 0, b: 3}, (data) => {
        data = JSON.parse(data);
        for (var post of data.posts) {
          post.content = post.content.substring(0, 100).trim() + "...";
          $("#bloglist").append(`
            <div class="item">
              <a href="blog/${post.id}">
                <h3>${post.title}</h3>
                <p>${post.content}</p>
              </a>
            </div>
          `);
        }
      });
    </script>
  </head>
  <body>
    <div id="overlay" style="z-index: 100; background-color: rgb(20, 20, 20);"></div>
    <div id="head">
      <p class="title gradienttext rainbow" topage=0 noglow>Paulo Silva</p>
      <div class="links">
        <div class="wrapper">
          <a class="gradienttext" href="#" topage=0>Home</a>
        </div>
        <div class="wrapper">
          <a class="gradienttext" href="#" topage=1>Portfolio</a>
        </div>
        <div class="wrapper">
          <a class="gradienttext" href="#" topage=2>Projets</a>
        </div>
        <div class="wrapper">
          <a class="gradienttext" href="#" topage=3>Actualités</a>
        </div>
      </div>
    </div>
    <div id="pages">
      <div id="home" class="page" page=0>
        <div class="gradients"></div>
        <div class="center-page">
          <div>
            <h1 id="a1-1" style="font-size: 90px;">Paulo Silva</h1>
            <p id="a1-2" style="width: 500px; font-size: 20px;">
               Un concepteur de sites Web, développeur, passionné de technologie a la recherche d'une Alternance. Pianiste a temps perdus, (généralement) une bonne personne.
            </p>
          </div>
          <br />
          <div>
            <a id="a1-3"><button topage=1>Mon portefolio!</button></a>
            <a id="a1-4"><button topage=2 style="background-color: #ff9f43;">Mes projets!</button></a>
            <!--<a id="a1-5" href="https://discord.gg/RgrRpZa" target="_blank"><button style="background-color: #7289da;">Discord!</button></a>-->
            <a id="a1-6" href="https://twitter.com/SANTOSDASILVAP1" target="_blank"><button style="background-color: #00a8ff;">Twitter!</button></a>
            <a id="a1-8" href="https://gitlab.com/mr.paulos97" target="_blank"><button style="background-color: #333;">GitLab!</button></a>
          </div>
        </div>
        <div id="a1-9" style="position: absolute; left: 50%; transform: translateX(-50%); text-align: center;">
          <p id="a1-10" style="color: white;">Scroll pour plus...</p>
          <img src="data/media/arrow.png" width="64" />
        </div>
      </div>
      <div id="portfolio" class="page" page=1>
        <div class="gradients"></div>
        <div class="center-page">
          <div>
            <h1>Portfolio</h1>
            <p>Bientôt disponible</p>
          </div>
          <div class="list" style="margin-left: 10px;">
          </div>
        </div>
      </div>
      <div id="blog" class="page" page=2>
        <div class="gradients"></div>
        <div class="center-page">
          <h1>Mes Projets</h1>
          <p>Bientôt disponible</p>
          <div id="bloglist">
          </div>
        </div>
      </div>
    </div>
    <div id="actualites" class="page" page=3>
        <div class="gradients"></div>
        <div class="center-page">
          <h1>Veille technologique</h1>
          <p>Bientôt disponible</p>
          <div id="bloglist">
          </div>
        </div>
      </div>
    </div>
    <div id="menus">
      <div class="menu loading-menu" menu=-1>
        <div class="wrapper" style="text-align: center;">
          <img id="loading" />
        </div>
      </div>
    </div>
  </body>
</html>
