ready(() => {
    for (var page of $("[page]"))
      if ($(page).attr("page") > Pages.total)
        Pages.total = $(page).attr("page");
  
    $("[topage]").click(function(event) {
      Pages.goTo($(event.target).attr("topage"));
    });
  
    Pages.updateGlow();
    setTimeout(() => {
      document.addEventListener("touchstart", touchStart);
      document.addEventListener("touchmove", touchMove);
      document.addEventListener("touchend", touchEnd);
  
      $(document).keydown(function(event) {
        if (!$.isEmptyObject(Pages.running) || isMenuOpen())
          return;
  
        if ((event.keyCode == 37 || event.keyCode == 38) && Pages.current - 1 >= 0)
          Pages.previousPage();
        if ((event.keyCode == 39 || event.keyCode == 40) && Pages.current + 1 <= Pages.total)
          Pages.nextPage();
      });
  
      $(document).on("DOMMouseScroll", scrolled);
      $(document).on("mousewheel", scrolled);
    }, 2000);
  });
  
  $(() => {
    var percent = 0;
    $("[page]").each((index, el) => {
      $(el).css("top", percent + "%");
      Pages.pages.push(new Page(el, percent));
      percent += 100;
    });
  });
  
  var yDown = null;
  var hasMoved = false;
  function touchStart(event) {
    yDown = (event.touches || event.originalEvent.touches)[0].clientY;
  }
  
  function touchEnd(event) {
    yDown = null;
    hasMoved = false;
  }
  
  function touchMove(event) {
    if (!yDown || isMenuOpen() || hasMoved)
      return;
  
    var yDiff = yDown - event.touches[0].clientY;
    if (Math.abs(yDiff) < 300)
      return;
  
    yDown = event.touches[0].clientY;
    hasMoved = true;
    if (yDiff > 0 && Pages.current + 1 <= Pages.total)
      Pages.nextPage();
    else if (yDiff < 0 && Pages.current - 1 >= 0)
      Pages.previousPage();
    else
      hasMoved = false;
  }
  
  var cooldown = 0;
  var previousDelta = 0;
  function scrolled(event) {
    var delta = event.originalEvent.detail * -1;
    if (delta == null || delta == 0)
      delta = event.originalEvent.wheelDelta;
  
    var realDelta = delta;
    delta = Math.abs(delta);
    if (previousDelta >= delta) {
      previousDelta = delta;
      return;
    }
  
    previousDelta = delta;
    if (!$.isEmptyObject(Pages.running) || cooldown != 0 || isMenuOpen())
      return;
  
    if (realDelta < 0 && Pages.current + 1 <= Pages.total)
      Pages.nextPage();
    else if (realDelta > 0 && Pages.current - 1 >= 0)
      Pages.previousPage();
    else
      return;
  
    cooldown = setTimeout(() => {
      cooldown = 0;
    }, Pages.cooldown);
  }
  
  // API
  var Pages = {
    pages: [],
    listeners: {
      onChange: [],
      changed: []
    },
    running: {},
    current: 0,
    total: 0,
    cooldown: 900,
  
    nextPage: function() {
      for (var page of Pages.pages) {
        page.setTop(page.getTop() - 100);
        page.element().css("top", page.getTop() + "%");
      }
      this.current++;
      this.updateGlow();
      this.pageChanged();
    },
    previousPage: function() {
      for (var page of Pages.pages) {
        page.setTop(page.getTop() + 100);
        page.element().css("top", page.getTop() + "%");
      }
      this.current--;
      this.updateGlow();
      this.pageChanged();
    },
    updateGlow: function() {
      $("[topage]:not([noglow])").addClass("grey");
      $("[topage]:not([noglow])").removeClass("rainbow");
      $(`[topage=${this.current}]:not([noglow])`).removeClass("grey");
      $(`[topage=${this.current}]:not([noglow])`).addClass("rainbow");
    },
    goTo: function(page) {
      for (var run in this.running)
        clearTimeout(this.running[run]);
  
      this.running = {};
      var forward = page > this.current;
      var times = Math.abs(page - this.current);
      for (var i = 0; i < times; i++) {
        var id = Math.random() * 100000;
        this.running[id] = setTimeout(function(id) {
          delete this.running[id];
          if (forward)
            this.nextPage();
          else
            this.previousPage();
        }.bind(this, id), i * (this.cooldown / 1.25));
      }
    },
    pageChanged: function() {
      var curPage = this.current;
      for (var listener of this.listeners.onChange)
        listener(curPage);
      setTimeout(() => {
        for (var listener of this.listeners.changed)
          listener(curPage);
      }, this.cooldown);
    },
    listen: function(type, func) {
      if (type == "changed")
        this.listeners.changed.push(func);
      if (type == "onchange")
        this.listeners.onChange.push(func);
    }
  };
  
  class Page {
    constructor(el, top) {
      this.el = el;
      this.top = top;
    }
  
    getTop() {
      return this.top;
    }
  
    setTop(top) {
      this.top = top;
    }
  
    element() {
      return $(this.el);
    }
  
    static getPage(el) {
      for (var page of Pages.pages)
        if (page.el == el)
          return page;
    }
  }
  
  ready(() => {
    setTimeout(() => {
      Pages.pageChanged();
    }, 1200);
  });
  