var timeouts = [];
Pages.listen("onchange", function(page) {
  stopAnimating();
  if (page == 0) {
    $("#a1-1").css({
      opacity: "0",
      left: "-20px"
    });

    $("#a1-2").css({
      opacity: "0",
      top: "20px"
    });

    $("#a1-3, #a1-4, #a1-5, #a1-6, #a1-7, #a1-8").css({
      opacity: "0",
      top: "20px"
    });

    $("#a1-9").css({
      bottom: "-50px"
    });

    $("#a1-10").css({
      opacity: "0"
    });
  }
});

Pages.listen("changed", function(page) {
  if (page == 0) {
    animate("#a1-1", {
      left: "0",
      opacity: "1"
    }, 0);

    animate("#a1-2", {
      top: "0",
      opacity: "1"
    }, 1000);

    for (var i = 3; i < 9; i++)
      animate("#a1-" + i, {
        top: "0",
        opacity: "1",
      }, 2500 + i * 100); // Max: 3200

    animate("#a1-9", {
      bottom: "0"
    }, 3500);

    animate("#a1-10", {
      opacity: "1"
    }, 4000);
  }
});

function stopAnimating() {
  for (var timeout of timeouts)
    clearTimeout(timeout);
  timeouts = [];
}

function animate(element, css, time) {
  timeouts.push(setTimeout(() => {
    $(element).css(css);
  }, time));
}
