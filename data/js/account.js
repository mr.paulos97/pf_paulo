if (window.back == null)
  var back = "";

var user = null;
$(() => {
  $("#head .links").append(`
    <div id="sep" class="sep"></div>
    <div id="account-wrapper" class="wrapper">
      <a id="account-txtbtn" class="rainbow gradienttext" style="cursor: pointer;">Log in</a>
      <img id="account-imgbtn" src="" />
    </div>
    `);

  $.get(back + "api/users/@me", {}, (res) => {
    res = JSON.parse(res);
    if (!res["error"])
      user = res;

    if (!user) {
      $("#account-txtbtn").css("display", "block");
      $("#sep").css("display", "inline-block");
    } else
      $("#account-imgbtn").attr("src", user.pp).on("load", () => {
        $("#account-imgbtn").css("display", "block");
        $("#sep").css("display", "inline-block");
        $("#account-wrapper").css("width", "27px");

        $("#account-in .title").html(user.username);
        $("#account-in #loggedin-pp").attr("src", user.pp);
      });
  });

  var open = false;
  $("#account-txtbtn, #account-imgbtn").click(() => {
    var change = open ? "none" : "block";
    if (user) {
      if (open)
        closeMenu(100);
      else
        openMenu(100);
    } else {
      if (open)
        closeMenu(101);
      else
        openMenu(101);
    }


    open = !open;
  });

  $("#login-name").focusout(() => {
    var user = $("#login-name").val().toLowerCase();
    console.log("GOING NOW");
    $.get(back + "api/users/" + user, {}, (res) => {
      res = JSON.parse(res);
      console.log(res);
      if (res["error"]) {
        $("#account-out .error").removeClass("disabled");
        $("#account-out .error").html("Oops! Invalid username.");
        return;
      }

      $("#login-pp").attr("src", res["pp"]).on("load", () => {
        $("#login-pp").removeClass("disabled");
        $("#login-name").addClass("darker");
      });
    });
  });

  $("#login-name, #login-pass").keyup((e) => {
    if (e.keyCode != 13)
      return;
    attemptLogin();
  });

  $("#login-name").keydown((e) => {
    if (e.keyCode == 9 || e.keyCode == 13)
      return;
    $("#account-out .error").addClass("disabled");
    $("#login-name").removeClass("darker");
    $("#login-pp").addClass("disabled");
  });

  function attemptLogin() {
    $("#account-out .error").addClass("disabled");
    var user = $("#login-name").val();
    var pass = $("#login-pass").val();
    $.post(back + "api/auth", {a: user, b: pass}, (res) => {
      if (JSON.parse(res)["error"]) {
        $("#account-out .error").removeClass("disabled");
        $("#account-out .error").html("Oops! Invalid credentials.");
      } else {
        $("#login-pass").addClass("darker");
        setTimeout(() => { location.reload(); }, 500);
      }
    });
  }

  function changeCredentials() {
    var user = $("#loggedin-name").val();
    var pass = $("#loggedin-pass").val();
    var pass2 = $("#loggedin-pass2").val();

    if (pass != pass2) {
      $("#account-in .error").removeClass("disabled");
      $("#account-in .error").html("Oops! Passwords don't match.");
      return;
    }

    $.post(back + "api/account/update", {a: user, b: pass}, (res) => {
      res = JSON.parse(res);
      if (res.error) {
        if (res.error.startsWith("U")) {
          $("#account-in .error").removeClass("disabled");
          $("#account-in .error").html("Oops! Username in use.");
        } else if (res.error.startsWith("I")) {
          $("#account-in .error").removeClass("disabled");
          $("#account-in .error").html("Oops! Username must be between 3-18 characters.");
        }
      } else {
        $("#account-in .error").removeClass("disabled");
        $("#account-in .error").html("Success! Updated.");
      }
    });
  }

  $("#login").click(attemptLogin);

  $("#logout").click(() => {
    $.post(back + "api/logout", {}, (res) => {
      location.reload();
    });
  });

  $("#loggedin-name, #loggedin-pass, #loggedin-pass2").keyup((e) => {
    if (e.keyCode != 13) {
      $("#account-in .error").addClass("disabled");
      return;
    }
    changeCredentials();
  });

  $("#updatecreds").click(changeCredentials);
});
