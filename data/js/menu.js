var opened = -1;
ready(function() {
  $("[openmenu]").click(function() {
    openMenu($(this).attr("openmenu"));
  });
  $("#overlay").click(closeMenu);
});

function openMenu(id, colour) {
  $("#overlay").css("z-index", "100");

  if (colour == "")
    colour = "rgba(0, 0, 0, 0.7)";

  $("#overlay").css("background", colour);
  $("#overlay").css("opacity", "1");
  $(`[menu=${id}]`).css("z-index", "101");
  $(`[menu=${id}]`).css("opacity", "1");
  opened = id;
}

function closeMenu() {
  $("#overlay").css("opacity", "0");
  setTimeout(() => { $("#overlay").css("z-index", "-100"); }, 500);

  $(`[menu=${opened}]`).css("opacity", "0");
  setTimeout(() => { $(`[menu=${opened}]`).css("z-index", "-1"); }, 500);

  $(`[menu=${opened}]`).css("z-index", "-1");
  opened = -1;
}

function isMenuOpen() {
  return opened != -1;
}
