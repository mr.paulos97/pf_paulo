var callbacks = [];
$(window).on("load", function() {
  for (var callback of callbacks)
    callback();

  setTimeout(() => {
    closeMenu();
  }, 2000);
});

function ready(func) {
  callbacks.push(func);
}

$(() => {
  var loadingImg = document.getElementById("loading");
  loadingImg.onload = function() {
    openMenu(-1, "#191b1b");
  };
  loadingImg.src = "data/media/loader.gif";
});
